package service_test

import (
	"testing"

	"domain/model"
	"domain/repository"
	"domain/service"
	"domain/utils"

	logs "domain/logger"

	"github.com/stretchr/testify/assert"
)

type MockRepository struct {
}

func newMockRepository() *MockRepository {
	return &MockRepository{}
}

var (
	mockRepository repository.Repository
)

func (m *MockRepository) Hello(req model.Request) (code int, message string) {
	return 200, "test"
}

func TestSendMail(t *testing.T) {

	config := model.AppConfig{
		AppInfo: model.AppInfo{
			Name: "test",
		},
		LoggerConfig: model.LoggerConfig{
			LogLevel: "info",
		},
	}
	logs.Init(config.AppInfo, config.LoggerConfig)
	logs := logs.LoggerSystem()

	testcases := []struct {
		name         string
		config       *model.AppConfig
		expectResult model.Response
	}{
		{
			name: "success",
			config: &model.AppConfig{
				AppInfo: model.AppInfo{
					Name:   "test",
					Banner: true,
				},
			},
			expectResult: model.Response{
				TimeStamp: utils.Now(),
				Code:      200,
				Message:   "test",
			},
		},
	}

	for _, testcase := range testcases {
		t.Run("testCase.name", func(t *testing.T) {
			req := model.Request{
				Name: "test",
			}
			mockRepository = newMockRepository()
			mockService := service.NewService(mockRepository)
			res := mockService.Hello(req, logs)
			assert.Equal(t, res.Message, testcase.expectResult.Message)
			assert.Equal(t, res.Code, testcase.expectResult.Code)
		})
	}
}
