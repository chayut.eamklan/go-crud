package repository

import (
	"fmt"
	"net/http"

	"domain/model"
)

type Repository interface {
	Hello(req model.Request) (code int, message string)
}

type repository struct {
}

func NewRepository() Repository {
	return &repository{}
}

func (r *repository) Hello(req model.Request) (code int, message string) {
	return http.StatusOK, fmt.Sprintf("Hello! %s", req.Name)
}
