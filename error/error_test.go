package error

import (
	"domain/model"
	"errors"
	"testing"

	log "domain/logger"

	"github.com/gofiber/fiber/v2"
	"github.com/stretchr/testify/assert"
	"github.com/valyala/fasthttp"
)

type Detail struct {
	Field       string
	Description string
}

func TestNewserverError(t *testing.T) {
	errmsg := "ข้อมูลไม่ถูกต้อง"
	detail := Detail{
		Field:       "ID",
		Description: "เป็นค่าว่าง",
	}
	want := ServerError{
		BaseError{
			Code:    "500",
			Message: errmsg,
			Detail:  detail,
		},
	}
	t.Run("test success", func(t *testing.T) {
		result := NewServerError(errmsg, detail)
		assert.Equal(t, want, result)
	})
}

func TestNewFieldValidationError(t *testing.T) {
	errmsg := "ข้อมูลไม่ถูกต้อง"
	want := ServerError{
		BaseError{
			Code:    "400",
			Message: errmsg,
			Detail:  nil,
		},
	}
	t.Run("test success", func(t *testing.T) {
		result := NewFieldValidationError(errmsg)
		assert.Equal(t, want, result)
		assert.Equal(t, want.BaseError.Error(), result.BaseError.Error())
	})
}

func TestHandler(t *testing.T) {
	config := model.AppConfig{
		AppInfo: model.AppInfo{
			Name: "test",
		},
		LoggerConfig: model.LoggerConfig{
			LogLevel: "info",
		},
	}
	errmsg := "ข้อมูลไม่ถูกต้อง"
	want := ServerError{
		BaseError{
			Code:    "400",
			Message: errmsg,
			Detail:  nil,
		},
	}
	log.Init(config.AppInfo, config.LoggerConfig)

	logger := log.LoggerSystem()
	handler := NewErrorResponseHandler(logger)

	handler.getStatusAndDetail(want)
}

func TestJsonMarshall(t *testing.T) {
	st, _ := jsonMarshal(`{
		"bucket": "cjexpress-membership-point-dev",
		"name": "membership-point_test_error1.csv",}`)

	assert.NotEmpty(t, st)

	invalidData := make(chan int) // chanel can't convert to json format
	_, err := jsonMarshal(invalidData)

	assert.Error(t, err)

}

func TestNewErrorResponse(t *testing.T) {
	errmsg := "ข้อมูลไม่ถูกต้อง"
	testError := ServerError{
		BaseError{
			Code:    "400",
			Message: errmsg,
			Detail:  nil,
		},
	}

	testError2 := BaseError{
		Code:    "400",
		Message: errmsg,
		Detail:  nil,
	}
	resultError1 := NewErrorResponse(testError)

	assert.NotEmpty(t, resultError1)

	resultError2 := NewErrorResponse(testError2)

	assert.NotEmpty(t, resultError2)

}

func TestHandlerError(t *testing.T) {

	code := "30201"
	msg := "Already paid"
	want := ServerError{
		BaseError{
			Code:    code,
			Message: msg,
		},
	}
	config := model.AppConfig{
		AppInfo: model.AppInfo{
			Name: "test",
		},
		LoggerConfig: model.LoggerConfig{
			LogLevel: "info",
		},
	}

	log.Init(config.AppInfo, config.LoggerConfig)
	logger := log.LoggerSystem()
	handler := NewErrorResponseHandler(logger)
	ctx := &fasthttp.RequestCtx{}

	// Send a response back to the client.
	ctx.SetContentType("text/plain")
	ctx.SetBodyString("Hello, World!")
	c := fiber.New().AcquireCtx(ctx)
	// c.App().Use(func(c *fiber.Ctx) error {
	// 	return c.Next()
	// }, handler.ErrorHandler)
	handler.ErrorHandler(c, want)
	handler.ErrorHandler(c, errors.New("test error default"))
	handler.ErrorHandler(c, fiber.ErrBadGateway)

}
