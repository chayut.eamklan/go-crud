# Use the official Golang base image
FROM golang:latest as builder

WORKDIR /app

# Copy the application source code into the container
COPY . .

# Build the Go application
RUN CGO_ENABLED=0 GOOS=linux go build -o main .

# Use the official Alpine base image for a smaller final image
FROM alpine:3.16.2

RUN apk --no-cache update && apk --no-cache add tzdata ca-certificates
ENV PATH="/app/:${PATH}"

ARG TIMEZONE="Asia/Bangkok"
RUN cp /usr/share/zoneinfo/${TIMEZONE} /etc/localtime && date

# Set the working directory inside the container
WORKDIR /app

# Copy the built Go application from the previous stage
COPY --from=builder /app/main .

COPY ./config/config.yaml /config/config.yaml

# Expose the port on which the application will run (if applicable)
EXPOSE 8083

# Set the command to run your application when the container starts
CMD ["./main"]
