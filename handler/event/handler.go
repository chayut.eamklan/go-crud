package event

import (
	"encoding/json"
	"time"

	"github.com/gofiber/fiber/v2"
	"github.com/google/uuid"

	"domain/model"
	"domain/service"
	v "domain/validation"

	"github.com/golang-jwt/jwt"

	"domain/utils"

	"github.com/sirupsen/logrus"
)

// interface
type HelloHandler interface {
	HelloHandler(c *fiber.Ctx) error
	LoginHandler(c *fiber.Ctx) error
}

// concrete implementation
type HelloHandlerImpl struct {
	logger  *logrus.Entry
	service *service.Service
}

// constructor
func NewHelloHandlerImpl(service *service.Service, logger *logrus.Entry) *HelloHandlerImpl {
	return &HelloHandlerImpl{
		logger:  logger,
		service: service,
	}
}

func (h *HelloHandlerImpl) HelloHandler(c *fiber.Ctx) error {
	req := model.Request{}
	if err := json.Unmarshal(c.Body(), &req); err != nil {
		return err
	}

	logs := h.logger.WithFields(logrus.Fields{
		"route":    c.Route().Path,
		"trace id": c.Get("X-Trace-Id", uuid.New().String()),
	})

	// validate
	if err := v.Validate(&req); err != nil {
		return err
	}

	logs.Info("Start Hello")

	res := h.service.Hello(req, logs)

	return c.Status(200).JSON(res)
}

func (h *HelloHandlerImpl) LoginHandler(c *fiber.Ctx) error {
	username := c.Query("username")
	token, err := createToken(username)
	if err != nil {
		return err
	}

	return c.Status(200).JSON(map[string]string{
		"access_token": token,
	})
}

func createToken(username string) (string, error) {
	token := jwt.NewWithClaims(jwt.SigningMethodHS256,
		jwt.MapClaims{
			"username": username,
			"exp":      time.Now().Add(time.Hour * 24).Unix(),
		})

	tokenString, err := token.SignedString(utils.GetSecretKey())
	if err != nil {
		return "", err
	}

	return tokenString, nil
}
