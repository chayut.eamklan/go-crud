package meta

import (
	_ "embed"

	"github.com/alexliesenfeld/health"
	"github.com/gofiber/adaptor/v2"
	"github.com/gofiber/fiber/v2"
)

type MetaHandler interface {
	Info() fiber.Handler
	Health() fiber.Handler
}

type MetaHandlerImpl struct {
	info               InfoResponse
	healthChecker      health.Checker
	healthCheckOptions health.HandlerOption
}

func NewMetaHandlerImpl(builtInfo string, healthCheckConfig HealthCheckConfig) MetaHandler {
	return &MetaHandlerImpl{
		info:               ParseInfo(builtInfo),
		healthChecker:      NewHealthChecker(healthCheckConfig),
		healthCheckOptions: health.WithResultWriter(&JsonResultWriter{}),
	}
}

func (this *MetaHandlerImpl) Info() fiber.Handler {
	return func(c *fiber.Ctx) error {
		return c.Status(200).JSON(this.info)
	}
}

func (this *MetaHandlerImpl) Health() fiber.Handler {
	return adaptor.HTTPHandlerFunc(health.NewHandler(this.healthChecker, this.healthCheckOptions))
}
