package main

import (
	"domain/config"
	//"gitlab.com/cjexpress/go-common/tracer"
)

func main() {
	// OpenTracing
	// closer, err := tracer.InitializeTracer("job-send-mail")
	// if err != nil {
	// 	panic(err)
	// }
	// defer closer.Close()

	apiServer := config.Bootstrap()
	defer config.Teardown()

	apiServer.Start()
}
