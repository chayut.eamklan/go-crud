package model

import (
	"domain/utils"
	"time"
)

type Request struct {
	Name string `json:"name,omitempty" validate:"required,notEmpty"`
	Test string `json:"-"`
}

type Response struct {
	TimeStamp *time.Time `json:"timestamp"`
	Code      int        `json:"code"`
	Message   string     `json:"message"`
}

func NewResponse(code int, message string) Response {
	return Response{
		TimeStamp: utils.Now(),
		Code:      code,
		Message:   message,
	}
}
