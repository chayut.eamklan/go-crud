package model

import "domain/handler/meta"

type AppConfig struct {
	AppInfo           AppInfo                `mapstructure:"app"`
	APIServerConfig   FiberConfig            `mapstructure:"apiServer"`
	LoggerConfig      LoggerConfig           `mapstructure:"logging"`
	HealthCheckConfig meta.HealthCheckConfig `mapstructure:"healthCheck"`
	Service           Service                `mapstructure:"service"`
	MongoConfig       MongoConfig            `mapstructure:"mongo"`
	CronjobConfig     CronjobConfig          `mapstructure:"cronjob"`
}

type AppInfo struct {
	Name   string `mapstructure:"name"`
	Banner bool   `mapstructure:"banner"`
}

type Service struct {
	BaseUrl      string `mapstructure:"baseUrl"`
	GetTokenUrl  string `mapstructure:"getTokenUrl"`
	ClientId     string `mapstructure:"clientId"`
	ClientSecret string `mapstructure:"clientSecret"`
	Scope        string `mapstructure:"scope"`
	Timeout      *int    `mapstructure:"timeoutSec"`
}

type FiberConfig struct {
	Name         string `mapstructure:"name"`
	Banner       bool   `mapstructure:"banner"`
	Address      string `mapstructure:"address"`
	ReadTimeout  int64  `mapstructure:"readTimeout"`
	WriteTimeout int64  `mapstructure:"writeTimeout"`
	IdleTimeout  int64  `mapstructure:"idleTimeout"`
}

type MongoConfig struct {
	URI                 string `mapstructure:"uri"`
	Database            string `mapstructure:"database"`
	MinPoolSize         uint64 `mapstructure:"minPoolSize"`
	MaxPoolSize         uint64 `mapstructure:"maxPoolSize"`
	MaxIdleTime         uint64 `mapstructure:"maxIdleTimeMilli"`
	ConnectTimeoutMilli uint64 `mapstructure:"connectTimeoutMilli"`
}

type LoggerConfig struct {
	LogLevel string `mapstructure:"level"`
}

type CronjobConfig struct {
	Format string `mapstructure:"format"`
}
