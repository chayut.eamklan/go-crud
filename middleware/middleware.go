package middleware

import (
	er "domain/error"
	"domain/utils"
	"fmt"
	"strings"

	"github.com/gofiber/fiber/v2"
	"github.com/golang-jwt/jwt"
)

func MiddelWare(c *fiber.Ctx) error {
	autoroize := c.Get("Authorization")
	if autoroize == "" {
		return er.NewDefinedError("401", "Unauthorization", nil)
	}
	tokenString := strings.Split(autoroize, " ")[1]
	if err := verifyToken(tokenString); err != nil {
		return err
	}
	return c.Next()
}

func verifyToken(tokenString string) error {
	token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
		return utils.GetSecretKey(), nil
	})

	if err != nil {
		return err
	}

	if !token.Valid {
		return fmt.Errorf("invalid token")
	}

	return nil
}
