package utils

import "time"

const (
	TimeFormat = "2006-01-02T15:04:05-07:00"
)

func Now() *time.Time {
	now, _ := time.Parse(TimeFormat, time.Now().Format(TimeFormat))
	return &now
}

func GetSecretKey() []byte {
	return []byte("secret-key")
}
